﻿using System;

namespace Ejercicio1
{
    class Program
    {
        static void Main(string[] args)

        {
            int a, b = 0;
            do
            {
                Console.Write("Ingresar digitos Positivos: ");
                a = Convert.ToInt32(Console.ReadLine());

                if (a > 0)
                {
                    Console.WriteLine("Valor valido.");
                    Console.WriteLine();
                    b = a + b;
                }
                if (a < -2)
                {
                    Console.WriteLine("Ingresar valor positivo.");
                    Console.WriteLine();
                }
            } while (a != 0);
            Console.WriteLine("El total de los digitos es: " + b);
            Console.ReadKey();
        }
    }
}
